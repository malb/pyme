#!/usr/bin/env python

# $Id$

# Module: installer
# COPYRIGHT #
# Copyright (C) 2016 Kai Michaelis <kai@gnupg.org>
#
#    This library is free software; you can redistribute it and/or
#    modify it under the terms of the GNU Lesser General Public
#    License as published by the Free Software Foundation; either
#    version 2.1 of the License, or (at your option) any later version.
#
#    This library is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#    Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public
#    License along with this library; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
# END OF COPYRIGHT #

import sys
import platform
import subprocess

def gpgme_config(what):
    try:
        process = subprocess.Popen(["gpgme-config", "--%s" % what],
                                   stdout=subprocess.PIPE)
        confdata = process.communicate()[0]
    except OSError as e:
         if e.errno == os.errno.ENOENT:
             raise RuntimeError("Could not call gpgme-config, perhaps install libgpgme-dev")
         else:
             raise
    confdata = confdata.replace("\n", " ")
    confdata = confdata.replace("  ", " ")
    return [x for x in confdata.split(' ') if x != '']

def gpg4win_dir():
    try:
        from _winreg import ConnectRegistry, QueryValueEx, OpenKey, EnumKey, HKEY_LOCAL_MACHINE
        reg = ConnectRegistry(None,HKEY_LOCAL_MACHINE)
        key = OpenKey(reg,r"SOFTWARE\WOW6432Node\GNU\GnuPG")
        val = QueryValueEx(key,"Install Directory")
        return val[0].encode().replace("\\","/")
    except WindowsError:
        return None

def gpgme_libs():
    if platform.system() == "Windows":
        maybe_gpg = gpg4win_dir()
        if maybe_gpg == None:
            raise RuntimeError("Could not find GNUpg, please install Gpg4Win")
        else:
            return ["libgpgme.imp","libgpg-assuan.imp","libgpg-error.imp","/LIBPATH:" + maybe_gpg + "/lib"]
    else: # *nix of MINGW
        return gpgme_config("libs")

def gpgme_prefix():
    if platform.system() == "Windows":
        maybe_gpg = gpg4win_dir()
        if maybe_gpg == None:
            raise RuntimeError("Could not find GNUpg, please install Gpg4Win")
        else:
            return [maybe_gpg]
    else: # *nix of MINGW
        return gpgme_config("exec-prefix")

def gpgme_cflags():
    if platform.system() == "Windows":
        maybe_gpg = gpg4win_dir()
        if maybe_gpg == None:
            raise RuntimeError("Could not find GNUpg, please install Gpg4Win")
        else:
            return ["-I" + maybe_gpg + "/include","-Iwin32/include"]
    else: # *nix of MINGW
        return gpgme_config("cflags")

for a in sys.argv[1:]:
    if a == "--libs":
        print " ".join(gpgme_libs()),
    elif a == "--cflags":
        print " ".join(gpgme_cflags()),
    elif a == "--exec-prefix":
        print gpgme_prefix(),
print ""

